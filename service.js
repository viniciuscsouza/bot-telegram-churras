import { join, dirname } from 'path'
import { LowSync, JSONFileSync } from 'lowdb'
import { fileURLToPath } from 'url'


const __dirname = dirname(fileURLToPath(import.meta.url));
const file = join(__dirname, 'db.json')

const adapter = new JSONFileSync(file)
const db = new LowSync(adapter)


export function gravar_dados(ctx){ 
  db.read()
  db.data = db.data || { eventos: [] }
  const { eventos } = db.data
  eventos.push(ctx)
  db.write()
  return eventos
}

export function ler_dados(){
  db.read()
  db.data = db.data || { eventos: [] }
  const { eventos } = db.data
  return eventos
}

export function atualiza_dados(ctx){
  db.read()
  db.data = db.data || { eventos: [] }
  db.data = ctx
  db.write()
}