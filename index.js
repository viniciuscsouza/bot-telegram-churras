import { Telegraf, Markup } from 'telegraf'
import Calendar from 'telegraf-calendar-telegram'
import dotenv from 'dotenv'
import { atualiza_dados, gravar_dados, ler_dados } from './service.js'

// Bot API Token
dotenv.config()
const bot = new Telegraf(process.env.BOT_TOKEN)

const churrasco = {
  "dia_do_churrasco" : null,
  "anfitriao": null,
  "participantes": [],
  "previsao_do_tempo": null,
  "insumos": {
    "carnes": [],
    "vegano": [],
    "acompanhamentos": [],
    "suprimentos": [],
    "bebidas": [],
  }
}

// Calendar config
const calendar = new Calendar(bot, {
	startWeekDay: 1,
	weekDayNames: ["S", "T", "Q", "Q", "S", "S", "D"],
	monthNames: [
		"Jan", "Fev", "Mar", "Abr", "Mai", "Jun",
		"Jul", "Ago", "Set", "Out", "Nov", "Dez"
	]
});

// Calendar Listener
calendar.setDateListener((ctx, date) => {
  churrasco.dia_do_churrasco = date
  // churrasco.anfitriao = ctx.update.message.from.first_name
  
  return ctx.reply("Confirma o churrasco no dia: " + date + "?", 
                    Markup.keyboard(["🆗 Confirmo", "⏹ Desisto"])
  )
})

bot.command('simple', (ctx) => {
  return ctx.replyWithHTML(
    '<b>Coke</b> or <i>Pepsi?</i>',
    Markup.keyboard(['Coke', 'Pepsi'])
  )
})

// Calendário HTML
bot.hears("🔥 Criar evento", ctx => {
	const today = new Date()
	const minDate = new Date()
	minDate.setMonth(today.getMonth())
	const maxDate = new Date()
	maxDate.setMonth(today.getMonth() + 2);
	maxDate.setDate(today.getDate());
	ctx.reply("Qual o dia do churrasco?", 
    calendar.setMinDate(minDate)
            .setMaxDate(maxDate)
            .getCalendar()
  )
});

bot.hears('🆗 Confirmo', (ctx) => {
  churrasco.anfitriao = ctx.message.from.first_name
  gravar_dados(churrasco)
  return ctx.reply("📌 Churrasco agendado")
})

bot.hears('☝️ Participar', (ctx) => {
  const eventos = ler_dados()
  const lista_eventos = eventos.map(evento => evento.dia_do_churrasco)
  return ctx.reply(
    'Encontrei esses eventos agendados, escolha um deles:',
    Markup.keyboard(lista_eventos).resize()
  )
})

bot.hears(/[0-9]{4}-[0-9]{2}-[0-9]{2}/, (ctx) => {
  let dia_escolhido = ctx.match[0]
  ctx.reply(`Oh, ${ctx.match[0]}! Ótima escolha`)
  let eventos = ler_dados()

  let novo_eventos = eventos.map(evento => {
    if(evento.dia_do_churrasco === dia_escolhido){
      evento["participantes"] = [...evento.participantes, ctx.from.first_name]
    }
    return evento
  })

  novo_eventos = { "eventos": novo_eventos }
  atualiza_dados(novo_eventos)
  eventos = ler_dados()
 
  // console.log(participantes)
  return ctx.reply('🤝')

})


//.then(ctx.reply('🤝'))

bot.hears('🔍 Check-In', (ctx) => {
  return ctx.reply(
    '🔍',
    Markup.keyboard([
      Markup.button.locationRequest('Enviar localização')
    ]).resize()
  )
})

bot.hears('⏹ Desisto', ctx => ctx.reply('Ooooooh 🥲'))

// bot.action("confirmar", (ctx, next) => {
//   gravar_dados(ctx)
//   ctx.answerCbQuery("📌 Churrasco agendado")
//   return next()
// })

// bot.action("desistir", (ctx) => {
  
//   return ctx.answerCbQuery("❌ Evento cancelado")
// })

// bot.hears("checkin", (ctx) => {
  
//   return ctx.reply(Markup.keyboard([
//     Markup.button.locationRequest("🌎 Enviar localização")
//   ])).resize()
// })



bot.use(Telegraf.log())

bot.command("churrasco", (ctx) =>
  ctx.reply("😋").then(
    ctx.reply('Opa! Diz aí o que você quer:', Markup
      .keyboard([
        ['🔥 Criar evento', '☝️ Participar', '🔍 Check-In'],
      ])
      .oneTime()
      .resize()
    )
  )
)

// bot.hears('🔥 Criar evento', ctx => ctx.reply("📌 Churrasco agendado"))


// bot.command('Novo', async (ctx) => {
//   return await ctx.reply()
// })

bot.command('custom', async (ctx) => {
  return await ctx.reply('Custom buttons keyboard', Markup
    .keyboard([
      ['🔍 Search', '😎 Popular'], // Row1 with 2 buttons
      ['☸ Setting', '📞 Feedback'], // Row2 with 2 buttons
      ['📢 Ads', '⭐️ Rate us', '👥 Share'] // Row3 with 3 buttons
    ])
    .oneTime()
    .resize()
  )
})

bot.hears('🔍 Search', ctx => ctx.reply('Yay!'))
bot.hears('📢 Ads', ctx => ctx.reply('Free hugs. Call now!'))

bot.command('special', (ctx) => {
  return ctx.reply(
    'Special buttons keyboard',
    Markup.keyboard([
      Markup.button.contactRequest('Send contact'),
      Markup.button.locationRequest('Send location')
    ]).resize()
  )
})

bot.command('pyramid', (ctx) => {
  return ctx.reply(
    'Keyboard wrap',
    Markup.keyboard(['one', 'two', 'three', 'four', 'five', 'six'], {
      wrap: (btn, index, currentRow) => currentRow.length >= (index + 1) / 2
    })
  )
})

bot.command('simple', (ctx) => {
  return ctx.replyWithHTML(
    '<b>Coke</b> or <i>Pepsi?</i>',
    Markup.keyboard(['Coke', 'Pepsi'])
  )
})

bot.command('inline', (ctx) => {
  return ctx.reply('<b>Coke</b> or <i>Pepsi?</i>', {
    parse_mode: 'HTML',
    ...Markup.inlineKeyboard([
      Markup.button.callback('Coke', 'Coke'),
      Markup.button.callback('Pepsi', 'Pepsi')
    ])
  })
})

bot.command('random', (ctx) => {
  return ctx.reply(
    'random example',
    Markup.inlineKeyboard([
      Markup.button.callback('Coke', 'Coke'),
      Markup.button.callback('Dr Pepper', 'Dr Pepper', Math.random() > 0.5),
      Markup.button.callback('Pepsi', 'Pepsi')
    ])
  )
})

bot.command('caption', (ctx) => {
  return ctx.replyWithPhoto({ url: 'https://picsum.photos/200/300/?random' },
    {
      caption: 'Caption',
      parse_mode: 'Markdown',
      ...Markup.inlineKeyboard([
        Markup.button.callback('Plain', 'plain'),
        Markup.button.callback('Italic', 'italic')
      ])
    }
  )
})

bot.hears(/\/wrap (\d+)/, (ctx) => {
  return ctx.reply(
    'Keyboard wrap',
    Markup.keyboard(['one', 'two', 'three', 'four', 'five', 'six'], {
      columns: parseInt(ctx.match[1])
    })
  )
})

bot.action('Dr Pepper', (ctx, next) => {
  return ctx.reply('👍').then(() => next())
})

bot.action('plain', async (ctx) => {
  await ctx.answerCbQuery()
  await ctx.editMessageCaption('Caption', Markup.inlineKeyboard([
    Markup.button.callback('Plain', 'plain'),
    Markup.button.callback('Italic', 'italic')
  ]))
})

bot.action('italic', async (ctx) => {
  await ctx.answerCbQuery()
  await ctx.editMessageCaption('_Caption_', {
    parse_mode: 'Markdown',
    ...Markup.inlineKeyboard([
      Markup.button.callback('Plain', 'plain'),
      Markup.button.callback('* Italic *', 'italic')
    ])
  })
})

bot.action(/.+/, (ctx) => {
  return ctx.answerCbQuery(`Oh, ${ctx.match[0]}! Great choice`)
})


bot.catch((err) => {
	console.log("Error in bot:", err);
});


bot.launch()

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'))
process.once('SIGTERM', () => bot.stop('SIGTERM'))


// // function criar_evento(ctx, usuario){
// //   ctx.reply(usuario + ", que dia é o churrasco?")
// // }

// bot.use(async (ctx, next) => {
//   console.time(`Processing update ${ctx.update.update_id}`)
//   await next() // runs next middleware
//   // runs after next middleware finishes
//   console.log(`Mensagem: ${ctx.update.message.from.first_name}`)
//   console.timeEnd(`Processing update ${ctx.update.update_id}`)
// })

// bot.command('churrasco', (ctx) => {
//   return ctx.reply(
//     'Vai levar o quê?',
//     Markup.inlineKeyboard([
//       Markup.button.callback('Carnes', 'Carnes'),
//       Markup.button.callback('Acompanhamentos', 'Acompanhamentos'),
//       Markup.button.callback('Suprimentos', 'Suprimentos'),
//       Markup.button.callback('Bebidas', 'Bebidas'),
//     ])
//   )
// })

// bot.on('text', (ctx) => {
//   const chat_id = ctx.update.message.chat.id
//   let usuario = ctx.update.message.from.first_name
//   let mensagem = ctx.update.message.text

//   // mensagem == "churrasco" && criar_evento(ctx, usuario)

//   // ctx.reply("Sua mensagem: " + mensagem)
//   // ctx.replyWithSticker('123123jkbhj6b')
//   // ctx.tg.sendDice(chat_id)
//   // ctx.reply(ctx.update.message.from.first_name)
//   // ctx.replyWithPhoto('https://picsum.photos/200/300/')
//   // ctx.replyWithPoll("O que levará?",["Boi", "Porco", "Ave"])
//   // ctx.replyWithQuiz("O que levará?",["Boi", "Porco", "Ave"])
//   //ctx.reply('Hello World')

// });

// bot.launch()

// // Enable graceful stop
// process.once('SIGINT', () => bot.stop('SIGINT'))
// process.once('SIGTERM', () => bot.stop('SIGTERM'))
